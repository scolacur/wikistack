var mongoose = require('mongoose');
var getSlug = require('speakingurl');

// Notice the `mongodb` protocol; Mongo is basically a kind of server,
// which handles database requests and sends responses. It's async!
mongoose.connect('mongodb://localhost/wikistack');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'mongodb connection error:'));


//A schema is a plan for what a single Mongo document (row) looks like
var pageSchema = new mongoose.Schema({
  title: {type: String, required: true},
  urlTitle: {type: String, required: true},
  content: {type: String, required: true},
  status: {type: String, enum: ['open', 'closed'], default: "open"},
  date: {type: Date, default: Date.now},
  author: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

pageSchema.pre('validate', function(next){
  this.urlTitle = generateTitle(this.title);
  console.log(this.urlTitle);
  next();
});

function generateTitle(title){
  if (!title){
    return Math.random().toString(36).substring(2,7);
  }
  console.log(getSlug(title));
  return getSlug(title);
}


pageSchema.virtual('route').get(function(){
  return "/wiki/"+ this.urlTitle;
});

var userSchema = new mongoose.Schema({
  name: {type: String, required: true},
  email: {type: String, required: true, unique: true}
});

userSchema.statics.findOrCreate = function (props) {
  var self = this;
  console.log("EMAIL",props.authorEmail);
  return self.findOne({email: props.authorEmail}).exec().then(function(user){
    if (user) return user;
    else {
      console.log("email and name: ",props.email, props.name);
      return self.create({
        email: props.authorEmail,
        name:  props.author
      });
    }
  });
};


var Page = mongoose.model('Page', pageSchema);
var User = mongoose.model('User', userSchema);



module.exports = {
  Page: Page,
  User: User
};

/*
This module will encapsulate our Mongoose initialization code.
Models:

Page
  title: The page's title
  urlTitle: a url-safe version of the page title, for links
  content: the page content
  date: when the page was authored
  status: if the page is open or closed
  author: which User wrote the page

USER

  name: JSON with first & last names
  email: a unique, identifying email address

Note

Mongoose schemas can store fields of the following types:

field: {type: String},
field: {type: Number},
field: {type: Date },
field: {type: Boolean},
field: {type: Array},
field: {type: Object},
field: {type: mongoose.Schema.Types.ObjectId, ref: 'Model'}
*/
