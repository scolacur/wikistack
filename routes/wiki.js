var express = require('express');
var router = express.Router();
var models = require('../models/');
var Page = models.Page;
var User = models.User;


/* Wiki Paths. */
router.get('/', function(req, res, next) {
  res.redirect('/');
//  res.send('respond with a resource');
});

router.get('/add', function(req, res, next) {
  res.render('addpage');
});


router.get("/:urlTitle", function (request, response, next) {
  var urlTitle = request.params.urlTitle;
  Page.findOne({urlTitle: urlTitle}).exec()
    .then(function (page) {
      console.log("the page:", page);
      if (page) {
        response.render("wikipage", page);
      } else {
        next();
      }
    }).then(null, next);
});


router.post('/', function(req, res, next) {
  console.log(JSON.stringify(req.body));

  var page = new Page({
    title: req.body.title,
    content: req.body.content
  });

  User.findOrCreate(req.body).then(function (user) {
    page.author = user._id;
    console.log('about to save page');
    return page.save();
  }).then(function (savedPage) {
    console.log("Page was saved!");
    res.redirect(savedPage.route);
  }).then(null, next);
});


module.exports = router;
