var express = require('express');
var router = express.Router();
var models = require("../models");
var Page = models.Page;


/* GET home page. */
router.get('/', function(req, res, next) {
	Page.find().exec().then(function (pages) {
		var pageContents = {
			title: "Home",
			pages: pages
		};
		res.render('index', pageContents);
	}).then(null, next);
});

module.exports = router;
